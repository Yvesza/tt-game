namespace TT.Domain.ViewModels
{
    public class ChatViewModel
    {
        public string RoomName { get; set; }
        public string ChatUser { get; set; }
        public string ChatColor { get; set; }
    }
}